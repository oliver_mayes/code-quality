<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@render');

Route::get('/random', 'HomeController@renderRandom');


Route::get('/kafka', 'KafkaController@view');
Route::post('/kafka', 'KafkaController@send');
