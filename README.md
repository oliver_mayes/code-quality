# code-quality

An example application for demonstrating Code Quality Standards

#### Setup
Clone this repo into a local directory

```
cd /apps/code-quality
docker-compose build
docker-compose up
```

SSH into the container

`docker exec -it code-quality-app /bin/bash`

Then, from inside the **code-quality-app** container

```
composer update
cp .env.example .env
php artisan key:generate
```

You should then be able to navigate to [localhost:8080](http://localhost:8080)


