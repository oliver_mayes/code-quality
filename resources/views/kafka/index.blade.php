@section('content')

    <style type="text/css">
        body{font-family: Arial;}
        div.holder{width: 80%; margin: 20px auto;}
        ul{width: 90%;}
        li{list-style-type: none;}
        table{width: 90%;}
    </style>
    <div class="holder">
        <h1>Kafka</h1>
            <form method="post">
                @csrf
                <ul>
                    <li><label for="message">Message: </label><input type="text" name="message" id="message"></li>
                    <li><input type="submit" /></li>
                </ul>
            </form>
            <table>
                <tr>
                    <th style="width: 80%">Message</th>
                    <th style="width: 20%">Date Created</th>
                </tr>
                @foreach ($messages as $msg)
                    <tr>
                        <td>{{ $msg->message }}</td>
                        <td>{{ $msg->created_at }}</td>
                    </tr>
                @endforeach

        </table>
    </div>