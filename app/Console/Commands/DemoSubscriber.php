<?php

namespace App\Console\Commands;

use App\Services\MessageService;
use Illuminate\Console\Command;
use Superbalist\PubSub\PubSubAdapterInterface;
use App\Services\KafkaService;

class DemoSubscriber extends Command
{
    /**
     * The name and signature of the subscriber command.
     *
     * @var string
     */
    protected $signature = 'demo:subscribe';

    /**
     * The subscriber description.
     *
     * @var string
     */
    protected $description = 'PubSub subscriber for Demo';

    /**
     * @var PubSubAdapterInterface
     */
    protected $pubsub;

    /**
     * @var string name of the kafka stream to use
     */
    protected $channel_name = 'demo_topic';

    /**
     * @var MessageService
     */
    protected $service;

    /**
     * Create a new command instance.
     *
     * @param PubSubAdapterInterface $pubsub
     */
    public function __construct(PubSubAdapterInterface $pubsub, MessageService $service)
    {
        parent::__construct();

        $this->pubsub = $pubsub;
        $this->service = $service;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->pubsub->subscribe($this->channel_name, function ($event) {
            $this->service->create($event);
        });
    }
}
