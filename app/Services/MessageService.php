<?php


namespace App\Services;

use App\Repositories\Message\MessageCache;

class MessageService
{
    protected $repo;

    public function __construct() {
        $this->repo = new MessageCache;
    }

    public function create($message) {
        $this->repo->create($message);
    }

    public function get($number = 20) {
        return $this->repo->get($number);
    }


}