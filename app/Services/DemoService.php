<?php

namespace App\Services;

use Illuminate\Http\Response;
use App\Repositories\RandomNumber\RandomNumberRepository;

class DemoService
{
    /**
     * @var RandomNumberRepository $repo
     */
    protected $repo;

    /**
     * DemoService constructor.
     * @param RandomNumberRepository $repo
     */
    public function __construct(RandomNumberRepository $repo) {
        $this->repo = $repo;
    }

    /**
     * Renders a demo page
     * @return Response
     */
    public function renderContent() {
        return $this->createResponse('Wibble');
    }

    /**
     * Renders a response containing a random number
     * @return Response
     */
    public function renderRandomNumber() {
        $rng = $this->repo->generateRandomNumber(time());
        $content = "Your random number is: ".$rng;
        return $this->createResponse($content);
    }

    /**
     * Creates a response object containing the provided content
     * @param String $content
     * @return Response
     */
    private function createResponse($content) {
        return response($content, 200)
            ->header('Content-Type', 'text/plain');
    }
}