<?php


namespace App\Services;

use App\Repositories\Kafka\KafkaMessage;

class KafkaService
{
    protected $repo;

    public function send($message = null) {
        $kafka = new KafkaMessage();
        $kafka->send($message);
    }
}