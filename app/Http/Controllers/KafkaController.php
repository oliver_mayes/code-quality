<?php


namespace App\Http\Controllers;

use App\Services\MessageService;
use Illuminate\Http\Request;
use App\Services\KafkaService;


class KafkaController extends Controller
{
    protected $kafkaService;

    protected $messageService;

    public function __construct(KafkaService $kafkaService, MessageService $messageService)
    {
        $this->kafkaService = $kafkaService;
        $this->messageService = $messageService;
    }

    public function view() {
        $messages = $this->messageService->get(20);
        return view('kafka.index', ['messages' => $messages]);
    }

    public function send(Request $request) {
       $this->kafkaService->send($request->input('message'));
       return $this->view();
    }
}