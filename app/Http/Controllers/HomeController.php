<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;

class HomeController
{
    /**
     * Stores the Demo Service
     *
     * @var $service
     */
    protected $service;

    public function __construct()
    {
        $this->service = resolve('DemoService');
    }

    /**
     * Default entry point
     *
     * @return Response
     */
    public function render() {
        return $this->service->renderContent();
    }

    /**
     * Advanced entry point
     *
     * @return Response
     */
    public function renderRandom() {
        return $this->service->renderRandomNumber();
    }
}