<?php

namespace App\Providers;

use App\Repositories\RandomNumber\RandomNumberRepository;
use App\Services\DemoService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('DemoService', function () {
            return new DemoService(new RandomNumberRepository());
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
      public function boot()
    {
        //
      }
}
