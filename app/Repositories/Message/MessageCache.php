<?php

namespace App\Repositories\Message;

use App\Models\Message;
use Illuminate\Support\Facades\DB;

class MessageCache
{
    public function create($message) {
        $newMessage = new Message;
        $newMessage->message = $message;
        $newMessage->save();
    }

    public function get($number) {
        return DB::table('message')->latest()->take($number)->get();
    }
}