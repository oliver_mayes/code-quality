<?php


namespace App\Repositories\Message;


interface Message
{
    public function create($message);

    public function get($number);
}