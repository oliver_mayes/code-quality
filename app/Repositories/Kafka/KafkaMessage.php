<?php


namespace App\Repositories\Kafka;


class KafkaMessage implements Kafka
{
    protected $channel_name = 'demo_topic';

    public function send($message) {
        $pubsub = app('pubsub');
        $pubsub->publish($this->channel_name, $message);
    }
}