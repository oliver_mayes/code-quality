<?php


namespace App\Repositories\Kafka;


interface Kafka
{
    public function send($message);
}