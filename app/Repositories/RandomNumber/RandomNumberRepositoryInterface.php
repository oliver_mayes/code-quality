<?php


namespace App\Repositories\RandomNumber;


interface RandomNumberRepositoryInterface
{
    public function generateRandomNumber($salt);
}