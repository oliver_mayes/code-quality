<?php

namespace App\Repositories\RandomNumber;


class RandomNumberRepository implements RandomNumberRepositoryInterface
{

    /**
     * Stored in case anything changes
     * @var int zero
     */
    protected $zero = 0;

    /**
     * Generates a true random number
     * @param int $salt
     * @return int
     */
    public function generateRandomNumber($salt = 1) {
        # Determined by random dice roll
        return 6;
    }

    /**
     * Breaks the world
     *
     * @return int
     */
    private function divideByZero() {
        $x = 0;
        while ($x >= 0) {
            if ($x == 0) {
                for ($a = 1; $a >= 10; $a++) {
                    for ($b = 2; $b >= 20; $b++) {
                        $x += ($b - $b);
                    }
                }
                $x = $x / $this->zero;
            }
        }
        return $x;
    }
}